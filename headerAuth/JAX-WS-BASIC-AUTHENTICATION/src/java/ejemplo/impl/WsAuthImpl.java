package ejemplo.impl;

import ejemplo.WsAuth;
import java.util.List;
import java.util.Map;
import javax.annotation.Resource;
import javax.jws.WebService;
import javax.xml.ws.WebServiceContext;
import javax.xml.ws.handler.MessageContext;

@WebService(endpointInterface = "ejemplo.WsAuth")
public class WsAuthImpl implements WsAuth {

    @Resource
    private WebServiceContext wsc;

    @Override
    public String authTest() {
        String result;
        
        MessageContext mc = wsc.getMessageContext();
        Map requestHeader = (Map) mc.get(MessageContext.HTTP_REQUEST_HEADERS);
        List userList = (List) requestHeader.get("Username");
        List passList = (List) requestHeader.get("Password");
        String username = "";
        String password = "";
        if (userList!=null && passList!=null){
            username = (String) userList.get(0);
            password = (String) passList.get(0);
        }
        if ("miguel".equals(username) && "1234".equals(password)){
            result = "bienvenido al servicio con autentiicacion"; 
        } else {
            result = "error mal ingreso";
        }
        return result;
    }

}
