package com.miguel.test.jax.ws.basic.auth.test;

import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.xml.ws.BindingProvider;
import javax.xml.ws.handler.MessageContext;
import test.WsAuth;
import test.WsAuthImplService;

public class Tester {
    
    public static void main(String[] args) {
        String service = "http://localhost:8080/JAX-WS-BASIC-AUTHENTICATION/WsAuthImpl?wsdl";
        WsAuthImplService wais = new WsAuthImplService();
        WsAuth soap = wais.getWsAuthImplPort();
        System.err.println(soap.authTest());
        Map<String, Object> reqMap = ((BindingProvider) soap).getRequestContext();
        reqMap.put(BindingProvider.ENDPOINT_ADDRESS_PROPERTY, service);
        Map<String, List<String>> header = new HashMap<>();
        header.put("Username", Collections.singletonList("miguel"));
        header.put("Password", Collections.singletonList("1234"));
        reqMap.put(MessageContext.HTTP_REQUEST_HEADERS, header);
        System.out.println(soap.authTest());
    }
            
}
