package com.miguel.test.tomcat.jaxws.container.auth;

import javax.jws.WebService;
import javax.jws.WebMethod;
import javax.jws.WebParam;

@WebService(serviceName = "SimpleWs")
public class SimpleWs {

    @WebMethod(operationName = "hello")
    public String hello(@WebParam(name = "name") String txt) {
        return "Hello Security " + txt + " !";
    }
}
