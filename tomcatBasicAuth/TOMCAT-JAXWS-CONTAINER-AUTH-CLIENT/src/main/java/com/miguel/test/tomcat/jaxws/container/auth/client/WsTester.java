package com.miguel.test.tomcat.jaxws.container.auth.client;

import com.miguel.test.tomcat.jaxws.container.auth.SimpleWs;
import com.miguel.test.tomcat.jaxws.container.auth.SimpleWs_Service;
import javax.xml.ws.BindingProvider;

public class WsTester {

    public static void main(String[] args) {
        SimpleWs ws = new SimpleWs_Service().getSimpleWsPort();
        BindingProvider bp = (BindingProvider) ws;
        bp.getRequestContext().put(BindingProvider.USERNAME_PROPERTY, "operatorUser");
        bp.getRequestContext().put(BindingProvider.PASSWORD_PROPERTY, "1234");
        String response = ws.hello("TESTER");
        System.out.println("Ws Response ==> " + response);
    }
    
}
